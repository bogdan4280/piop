package repos;

import org.eclipse.collections.impl.bag.mutable.HashBag;

public class HashBagBasedRepository<E> implements InMemoryRepository<E> {
    private final HashBag<E> hashBag;

    public HashBagBasedRepository() {
        this.hashBag = new HashBag<E>();
    }

    @Override
    public void add(E elem) {
        hashBag.add(elem);
    }

    @Override
    public boolean contains(E elem) {
        return hashBag.contains(elem);
    }

    @Override
    public void remove(E elem) {
        hashBag.remove(elem);
    }
}
