package repos;

import java.util.HashSet;

public class HashSetBasedRepository<E> implements InMemoryRepository<E> {
    private final HashSet<E> hashSet;

    public HashSetBasedRepository() {
        this.hashSet = new HashSet<E>();
    }

    @Override
    public void add(E elem) {
        hashSet.add(elem);
    }

    @Override
    public boolean contains(E elem) {
        return hashSet.contains(elem);
    }

    @Override
    public void remove(E elem) {
        hashSet.remove(elem);
    }
}
