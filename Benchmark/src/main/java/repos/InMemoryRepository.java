package repos;

public interface InMemoryRepository<E> {
    void add(E elem);
    boolean contains(E elem);
    void remove(E elem);
}


