package repos;

import org.eclipse.collections.impl.list.mutable.FastList;

public class FastListBasedRepository<E> implements InMemoryRepository<E>{
    private final FastList<E> fastList;

    public FastListBasedRepository() {
        this.fastList = new FastList<E>();
    }

    @Override
    public void add(E elem) {
        fastList.add(elem);
    }

    @Override
    public boolean contains(E elem) {
        return fastList.contains(elem);
    }

    @Override
    public void remove(E elem) {
        fastList.remove(elem);
    }
}
