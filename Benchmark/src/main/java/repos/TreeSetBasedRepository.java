package repos;

import java.util.TreeSet;

public class TreeSetBasedRepository<E> implements InMemoryRepository<E>  {
    private final TreeSet<E> treeSet;

    public TreeSetBasedRepository() {
        this.treeSet = new TreeSet<E>();
    }

    @Override
    public void add(E elem) {
        treeSet.add(elem);
    }

    @Override
    public boolean contains(E elem) {
        return treeSet.contains(elem);
    }

    @Override
    public void remove(E elem) {
        treeSet.remove(elem);
    }

}
