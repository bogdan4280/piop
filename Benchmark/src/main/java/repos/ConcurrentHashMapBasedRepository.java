package repos;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<K, V> {
    private final ConcurrentHashMap<K, V> concurrentHashMap;

    public ConcurrentHashMapBasedRepository() {
        this.concurrentHashMap = new ConcurrentHashMap<K, V>();
    }

    public void add(K key, V value) {
        concurrentHashMap.put(key, value);
    }

    public boolean contains(V value) {
        return concurrentHashMap.contains(value);
    }

    public void remove(V value) {

    }
}
