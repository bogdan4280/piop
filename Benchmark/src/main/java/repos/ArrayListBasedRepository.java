package repos;

import java.util.ArrayList;

public class ArrayListBasedRepository<E> implements InMemoryRepository<E>{
    private final ArrayList<E> arrayList;

    public ArrayListBasedRepository() {
        this.arrayList = new ArrayList<E>();
    }

    @Override
    public void add(E elem) {
        arrayList.add(elem);
    }

    @Override
    public boolean contains(E elem) {
        return arrayList.contains(elem);
    }

    @Override
    public void remove(E elem) {
        arrayList.remove(elem);
    }

    public int size(){
        return arrayList.size();
    }


}
