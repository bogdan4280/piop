package benchmark;

import org.openjdk.jmh.annotations.*;
import repos.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ContainsBenchmarks {
    private static final Collection<String> items = new ArrayList<>();

    public ContainsBenchmarks() {
        for (int i=0; i<10000; i++) {
            items.add("item" + i);
        }
        Collections.shuffle((List<?>) items);
    }

    @State(Scope.Benchmark)
    public static class RepoState {

        ArrayListBasedRepository<String> arrayList = new ArrayListBasedRepository<>();
        FastListBasedRepository<String> fastList = new FastListBasedRepository<>();
        HashBagBasedRepository<String> hashBag = new HashBagBasedRepository<>();
        HashSetBasedRepository<String> hashSet = new HashSetBasedRepository<>();
        TreeSetBasedRepository<String> treeSet= new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Integer, String> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();

        @Setup(Level.Trial)
        public void setup() {
            for(String item:items){
                arrayList.add(item);
                fastList.add(item);
                hashBag.add(item);
                hashSet.add(item);
                treeSet.add(item);
                concurrentHashMap.add(Integer.parseInt(String.valueOf(item.charAt(item.length()-1))), item);
            }
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_arrayList(RepoState state) {
        for (String item:items){
            state.arrayList.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_fastList(RepoState state) {
        for (String item : items) {
            state.fastList.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_hashBag(RepoState state) {
        for (String item:items){
            state.hashBag.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_hashSet(RepoState state) {
        for (String item:items){
            state.hashSet.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_treeSet(RepoState state) {
        for (String item:items){
            state.treeSet.contains(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void contains_concurrentHashMap(RepoState state) {
        for (String item:items){
            state.concurrentHashMap.contains(item);
        }
    }
}
