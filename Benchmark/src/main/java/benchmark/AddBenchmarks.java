package benchmark;

import org.openjdk.jmh.annotations.*;
import repos.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddBenchmarks {
    private static final Collection<String> items = new ArrayList<>();

    public AddBenchmarks() {
        for (int i=0; i<10000; i++) {
            items.add("item" + i);
        }
        Collections.shuffle((List<?>) items);
    }

    @State(Scope.Benchmark)
    public static class RepoState {

        ArrayListBasedRepository<String> arrayList = new ArrayListBasedRepository<>();
        FastListBasedRepository<String> fastList = new FastListBasedRepository<>();
        HashBagBasedRepository<String> hashBag = new HashBagBasedRepository<>();
        HashSetBasedRepository<String> hashSet = new HashSetBasedRepository<>();
        TreeSetBasedRepository<String> treeSet= new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Integer, String> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();

        @Setup(Level.Invocation)
        public void setup() {
            for(String item:items){
                arrayList.remove(item);
                fastList.remove(item);
                hashBag.remove(item);
                hashSet.remove(item);
                treeSet.remove(item);
                concurrentHashMap.remove(item);
            }
        }

    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_arrayList(RepoState state) {
        for (String item : items) {
            state.arrayList.add(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_fastList(RepoState state) {
        for (String item : items) {
            state.fastList.add(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_hashBag(RepoState state) {
        for (String item:items){
            state.hashBag.add(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_hashSet(RepoState state) {
        for (String item:items){
            state.hashSet.add(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_treeSet(RepoState state) {
        for (String item:items){
            state.treeSet.add(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_concurrentHashMap(RepoState state) {
        for (String item:items){
            state.concurrentHashMap.add(Integer.parseInt(String.valueOf(item.charAt(item.length()-1))), item);
        }
    }


}