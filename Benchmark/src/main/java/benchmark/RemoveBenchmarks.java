package benchmark;

import org.openjdk.jmh.annotations.*;
import repos.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RemoveBenchmarks {

    private static final Collection<String> items = new ArrayList<>();

    public RemoveBenchmarks() {
        for (int i=0; i<10000; i++) {
            items.add("item" + i);
        }
        Collections.shuffle((List<?>) items);
    }

    @State(Scope.Benchmark)
    public static class InheritanceState {

        ArrayListBasedRepository<String> arrayList = new ArrayListBasedRepository<>();
        FastListBasedRepository<String> fastList = new FastListBasedRepository<>();
        HashBagBasedRepository<String> hashBag = new HashBagBasedRepository<>();
        HashSetBasedRepository<String> hashSet = new HashSetBasedRepository<>();
        TreeSetBasedRepository<String> treeSet= new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Integer, String> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();

        @Setup(Level.Invocation)
        public void setup() {
            for(String item:items){
                arrayList.add(item);
                fastList.add(item);
                hashBag.add(item);
                hashSet.add(item);
                treeSet.add(item);
                concurrentHashMap.add(Integer.parseInt(String.valueOf(item.charAt(item.length()-1))), item);
            }
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_arrayList(ContainsBenchmarks.RepoState state) {
        for (String item:items){
            state.arrayList.remove(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_fastList(ContainsBenchmarks.RepoState state) {
        for (String item : items) {
            state.fastList.remove(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_hashBag(ContainsBenchmarks.RepoState state) {
        for (String item:items){
            state.hashBag.remove(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_hashSet(ContainsBenchmarks.RepoState state) {
        for (String item:items){
            state.hashSet.remove(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_treeSet(ContainsBenchmarks.RepoState state) {
        for (String item:items){
            state.treeSet.remove(item);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.SingleShotTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void remove_concurrentHashMap(ContainsBenchmarks.RepoState state) {
        for (String item:items){
            state.concurrentHashMap.remove(item);
        }
    }


}
