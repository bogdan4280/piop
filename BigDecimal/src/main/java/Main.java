import com.google.protobuf.ByteString;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static BigDecimal sum(List<BigDecimal> numbers)
    {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal mean(List<BigDecimal> numbers)
    {
        BigDecimal[] totalWithCount
                = numbers.stream()
                .map(bd -> new BigDecimal[]{bd, BigDecimal.ONE})
                .reduce((a, b) -> new BigDecimal[]{a[0].add(b[0]), a[1].add(BigDecimal.ONE)})
                .get();
        return  totalWithCount[0].divide(totalWithCount[1], 2, RoundingMode.HALF_UP);
    }

    public static List<BigDecimal> listTop(List<BigDecimal> numbers)
    {
        int count = numbers.size();
        int limit = Math.floorDiv(count, 10);
        return numbers.stream().sorted(Comparator.reverseOrder())
                .limit(limit)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<BigDecimal> numbers = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<10000; i++){
            double nthRandomNumber = random.nextDouble(0,100);
            numbers.add(BigDecimal.valueOf(nthRandomNumber));
        }

        System.out.println("Sum:" + sum(numbers));
        System.out.println("Mean:" + mean(numbers));
        System.out.println("Top 10%:");
        listTop(numbers).forEach(System.out::println);

        for (int i = 0; i < numbers.size(); i++) {

            DecimalValueOuterClass.DecimalValue serialized = DecimalValueOuterClass.DecimalValue.newBuilder()
                    .setScale(numbers.get(i).scale())
                    .setPrecision(numbers.get(i).precision())
                    .setValue(ByteString.copyFrom(numbers.get(i).unscaledValue().toByteArray()))
                    .build();

            java.math.MathContext mc = new java.math.MathContext(serialized.getPrecision());

            java.math.BigDecimal deserialized = new java.math.BigDecimal(
                    new java.math.BigInteger(serialized.getValue().toByteArray()),
                    serialized.getScale(),
                    mc);

            if(!numbers.get(i).equals(deserialized)){
                System.out.println("Serialization isn't correct for:" + numbers.get(i));
            }
        }

    }
}
