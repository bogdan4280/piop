import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * JUnit test for the money bag class.
 */
public class BigDecimalTest {
    private List<BigDecimal> numbers;

    @Before
    public void initialize() {
         this.numbers = new ArrayList<BigDecimal>();
         numbers.add(BigDecimal.valueOf(2));
         numbers.add(BigDecimal.valueOf(3));
         numbers.add(BigDecimal.valueOf(4));
         numbers.add(BigDecimal.valueOf(5));
    }

    @Test
    public void testSum() {
        Assert.assertEquals(Main.sum(numbers), new BigDecimal("15"));
        numbers.add(BigDecimal.valueOf(6));
        Assert.assertEquals(Main.sum(numbers), new BigDecimal("21"));

    }

    @Test
    public void testMean() {
        Assert.assertEquals(Main.mean(numbers), new BigDecimal("3.00"));
        numbers.add(BigDecimal.valueOf(6));
        Assert.assertEquals(Main.mean(numbers), new BigDecimal("3.50"));
    }

    @Test
    public void test10Percent() {
        for(int i=0;i<36;i++){
            numbers.add(BigDecimal.ONE);
        }
        Assert.assertEquals(numbers.size(), 40);
        List<BigDecimal> listTop = Main.listTop(numbers);
        Assert.assertEquals(listTop.size(), 4);
        for(int i=0;i<listTop.size();i++) {
            Assert.assertEquals(listTop.get(i), BigDecimal.valueOf(5-i));
        }
    }

}